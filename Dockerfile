#FROM openjdk:17
#COPY target/FirstProject-0.0.1-SNAPSHOT.jar /app.jar
#ENTRYPOINT ["java", "-jar", "/app.jar"]
#EXPOSE 8080

FROM openjdk:17-jdk
#ARG JAR_FILE=target/*.jar
COPY target/*.jar /app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
EXPOSE 8080